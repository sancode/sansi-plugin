package cn.sansi.plugin.demo;

import cn.sansi.plugin.AbstractServicePlugin;
import com.alibaba.fastjson2.JSON;
import lombok.extern.slf4j.Slf4j;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author 林芳崽
 * @date 2024/3/11
 */
@Slf4j
public class DemoServicePugin extends AbstractServicePlugin {
    @Override
    public Map<String, Object> call(Map<String, Object> args) {
        Map<String,Object> result=new LinkedHashMap<>();
        result.putAll(args);
        result.put("code","success");
        log.error(JSON.toJSONString(args));
        DemoResult demoResult=new DemoResult();
        demoResult.setCode("200");
        demoResult.setValue("操作成功");
        result.put("data",demoResult);
        return result;
    }
}
