package cn.sansi.plugin.demo;

import lombok.Data;

/**
 * @author 林芳崽
 * @date 2024/3/11
 */
@Data
public class DemoResult {
    private String code;
    private String value;
}
