package cn.sansi.plugin;

import java.util.Map;

/**
 * @author 林芳崽
 * @date 2024/3/11
 */
public abstract class AbstractServicePlugin extends AbstractPlugin{

    /**
     * 参数列表
     * @param args 参数
     */
   public abstract Map<String,Object> call(Map<String,Object> args);
}
