package cn.sansi.plugin;

/**
 * 插件定义
 */
public interface Pluggable extends Cloneable{
	String getDeveloper();

    String getDescription();

    void setPluginConf(Configuration pluginConf);

	void init();

	void destroy();

    String getPluginName();

    Configuration getPluginConf();

}
