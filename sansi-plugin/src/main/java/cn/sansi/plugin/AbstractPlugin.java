package cn.sansi.plugin;


import cn.sansi.common.base.BaseObject;

import java.util.Map;

/**
 * 插件抽象基类
 */
public abstract class AbstractPlugin extends BaseObject implements Pluggable {
    //插件本身的plugin
	private Configuration pluginConf;

    @Override
	public String getPluginName() {
		assert null != this.pluginConf;
		return this.pluginConf.getString("name");
	}

    @Override
	public String getDeveloper() {
		assert null != this.pluginConf;
		return this.pluginConf.getString("developer");
	}

    @Override
	public String getDescription() {
		assert null != this.pluginConf;
		return this.pluginConf.getString("description");
	}

    @Override
	public Configuration getPluginConf() {
		return pluginConf;
	}

    @Override
	public void setPluginConf(Configuration pluginConf) {
		this.pluginConf = pluginConf;
	}

	@Override
	public void init() {

	}

	@Override
	public void destroy() {

	}
}
