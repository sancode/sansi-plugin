package cn.sansi.plugin.container;

import org.junit.jupiter.api.Assertions;

/**
 * @author sansi
 * @version 2024/3/8
 */
public class LoadUtilTest {

    @org.junit.jupiter.api.Test
    public void loadPlugin() throws ClassNotFoundException {
        String path="D:\\datax\\plugin\\reader\\mysqlreader";
        JarLoader jarLoader =new JarLoader(new String[]{path});
        Assertions.assertNotNull(jarLoader);
        Class classz = jarLoader.loadClass("com.alibaba.datax.plugin.reader.mysqlreader.MysqlReader");
        Assertions.assertNotNull(classz);
        Class classJob = jarLoader.loadClass("com.alibaba.datax.plugin.reader.mysqlreader.MysqlReader$Job");
        Assertions.assertNotNull(classJob);
        Class classTask = jarLoader.loadClass("com.alibaba.datax.plugin.reader.mysqlreader.MysqlReader$Task");
        Assertions.assertNotNull(classTask);
    }
}