package cn.sansi.plugin.container;

import cn.sansi.plugin.Engine;
import com.alibaba.fastjson2.JSON;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 林芳崽
 * @date 2024/3/11
 */
public class EngineTest {

    @Test
    public   void testServiceTask( ){
        String pluginName = "demotask";
        LoadUtil.setPluginHomePath("E:\\studyws\\sansi-plugin\\sansi-plugin-demo\\target\\plugins");
        Map<String, Object> args = new HashMap<String, Object>();
        args.put("name", pluginName);
        args.put("linfangzai","222");
        TaskArg arg = new TaskArg();
        arg.setValue("300");
        arg.setName("taskArg");
        args.put("task", arg);
        Map<String, Object> result=  Engine.callService(pluginName,"",args);
        System.out.println(JSON.toJSONString(result));
        System.out.println(result.getClass().getName());
    }

    public static class TaskArg {
        private String name;
        private String value;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
