package cn.sansi.common.exception;


import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * DataX运行时异常
 * @author sancode
 */
public class BizException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private ErrorCode errorCode;

    public BizException(ErrorCode errorCode, String errorMessage) {
        super(errorCode.toString() + " - " + errorMessage);
        this.errorCode = errorCode;
    }

    public BizException(String errorMessage) {
        super(errorMessage);
    }

    private BizException(ErrorCode errorCode, String errorMessage, Throwable cause) {
        super(errorCode.toString() + " - " + getMessage(errorMessage) + " - " + getMessage(cause), cause);

        this.errorCode = errorCode;
    }

    public static BizException asBizException(ErrorCode errorCode, String message) {
        return new BizException(errorCode, message);
    }

    public static BizException asBizException(String message) {
        return new BizException(message);
    }

    public static BizException asBizException(ErrorCode errorCode, String message, Throwable cause) {
        if (cause instanceof BizException) {
            return (BizException) cause;
        }
        return new BizException(errorCode, message, cause);
    }

    public static BizException asBizException(ErrorCode errorCode, Throwable cause) {
        if (cause instanceof BizException) {
            return (BizException) cause;
        }
        return new BizException(errorCode, getMessage(cause), cause);
    }

    public ErrorCode getErrorCode() {
        return this.errorCode;
    }

    private static String getMessage(Object obj) {
        if (obj == null) {
            return "";
        }

        if (obj instanceof Throwable) {
            StringWriter str = new StringWriter();
            PrintWriter pw = new PrintWriter(str);
            ((Throwable) obj).printStackTrace(pw);
            return str.toString();
            // return ((Throwable) obj).getMessage();
        } else {
            return obj.toString();
        }
    }
}
