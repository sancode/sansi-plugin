# java-study
利用Jarloader 打造自己的插件机制

1、修改插件加载机制
修改jarloader构建方法，实现根据插件名称加载插件

```java
private static synchronized Class<? extends AbstractPlugin> loadPluginClass(Configuration config) {
    JarLoader jarLoader = LoadUtil.getJarLoader(config.getString("name"));
    try {
        return (Class<? extends AbstractPlugin>) jarLoader
                .loadClass(config.getString("class"));
    } catch (Exception e) {
        throw BizException.asBizException(PluginErrorCode.RUNTIME_ERROR, e);
    }
}
```

2、定义插件基类

```
public abstract class AbstractServicePlugin extends AbstractPlugin{
   public abstract Map<String,Object> call(Map<String,Object> args);
}
```

3、插件调用封装
Datax 通过 AbstractContainer 封装了插件调用逻辑，我们这里将调用逻辑封装如下：


```
public static Map<String, Object> callService(String pluginName,String configValue, Map<String, Object> args) {
    AbstractPlugin plugin = LoadUtil.loadPlugin(pluginName,configValue);
    if (plugin instanceof AbstractServicePlugin) {
        ClassLoaderSwapper classLoaderSwapper = context.get();
        JarLoader jarLoader= LoadUtil.getJarLoader(pluginName);
        try {
            classLoaderSwapper.setCurrentThreadClassLoader(jarLoader);
            return ((AbstractServicePlugin) plugin).call(args);
        }finally {
            classLoaderSwapper.restoreCurrentThreadClassLoader();
        }
    } else {
        throw BizException.asBizException(PluginErrorCode.INSTALL_ERROR, "插件类型无效");
    }
}
```

4、编写插件DEMO编写
插件使用 assembly打包机制，pom文件添加 assembly 插件

```

<plugin>
    <artifactId>maven-assembly-plugin</artifactId>
    <configuration>
        <descriptors>
            <descriptor>src/main/assembly/package.xml</descriptor>
        </descriptors>
        <finalName>plugins</finalName>
    </configuration>
    <executions>
        <execution>
            <id>dwzip</id>
            <phase>package</phase>
            <goals>
                <goal>single</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

其中package.xml包括 plugin.json、jar、lib等资源打包，具体定义文件如下：

```
<assembly
        xmlns="http://maven.apache.org/plugins/maven-assembly-plugin/assembly/1.1.0"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://maven.apache.org/plugins/maven-assembly-plugin/assembly/1.1.0 http://maven.apache.org/xsd/assembly-1.1.0.xsd">
    <id></id>
    <formats>
        <format>dir</format>
    </formats>
    <includeBaseDirectory>false</includeBaseDirectory>
    <fileSets>
        <fileSet>
            <directory>src/main/resources</directory>
            <includes>
                <include>plugin.json</include>
            </includes>
            <outputDirectory>demotask</outputDirectory>
        </fileSet>
        <fileSet>
            <directory>target/</directory>
            <includes>
                <include>sansi-plugin-demo-1.0-SNAPSHOT.jar</include>
            </includes>
            <outputDirectory>demotask</outputDirectory>
        </fileSet>
    </fileSets>
    <dependencySets>
        <dependencySet>
            <useProjectArtifact>false</useProjectArtifact>
            <outputDirectory>demotask/libs</outputDirectory>
            <scope>runtime</scope>
        </dependencySet>
    </dependencySets>
</assembly>
```

编辑插件服务，实现 日志输出、返回结果等。

```
@Slf4j
public class DemoServicePugin extends AbstractServicePlugin {
    @Override
    public Map<String, Object> call(Map<String, Object> args) {
        Map<String,Object> result=new LinkedHashMap<>();
        result.putAll(args);
        result.put("code","success");
        log.error(JSON.toJSONString(args));
        DemoResult demoResult=new DemoResult();
        demoResult.setCode("200");
        demoResult.setValue("操作成功");
        result.put("data",demoResult);
        return result;
    }
}
```

插件编译
